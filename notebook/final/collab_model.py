from utils import get_ratings_and_links_merged, reverse_dict, get_users_and_movies_count, get_all_data_merged


class CollaborativeModel(object):

    def __init__(self):
        print 'Loading ratings and links...'
        self.ratings_and_links = get_ratings_and_links_merged()

        print 'Loading all data...'
        self.all_data = get_all_data_merged()

        print 'Loading movie id <-> IMDB id translations...'
        self.n_users, self.n_movies = get_users_and_movies_count(self.ratings_and_links)
        self.movie_id_to_imdb_id_from_all_data = self.__get_movie_id_to_imdb_id()
        self.imdb_id_to_movie_id_from_all_data = reverse_dict(self.movie_id_to_imdb_id_from_all_data)

        self.deep_model = None
        print 'Finished!'

    def load_model(self, deep_model, model_id):
        print 'Loading model...'
        self.deep_model = deep_model
        self.deep_model.load_weights(model_id)
        print 'Finished!'

    def get_movies_rated_by(self, all_data, user_id, rate_threshold='0.0'):
        ratings_by_user = all_data.loc[all_data['userId'] == user_id]
        if rate_threshold != '0.0':
            ratings_by_user = ratings_by_user.loc[ratings_by_user['rating'] > str(rate_threshold)]
        movie_ids = ratings_by_user.movieId.unique()
        return movie_ids, map(lambda x: self.movie_id_to_imdb_id_from_all_data[x], movie_ids), ratings_by_user

    def get_movies_not_rated_by(self, all_data, user_id):
        rated_movies, _, _ = self.get_movies_rated_by(all_data, user_id)
        return [m_id for m_id in xrange(0, self.n_movies) if m_id not in rated_movies]

    def get_movie_rates(self, user_id, movies_ids):
        return {m_id: self.deep_model.rate(user_id, m_id) for m_id in movies_ids}

    def get_movie_rates_by_imdb_ids(self, user_id, imdb_ids):
        return {imdb_id: self.deep_model.rate(user_id, self.imdb_id_to_movie_id_from_all_data[imdb_id]) for
                imdb_id in imdb_ids}

    def get_all_movie_recommendations(self, all_data, user_id):
        movie_id_to_title = {row.movieId: row.title for row in all_data.itertuples()}
        movies = self.get_movies_not_rated_by(all_data, user_id)
        return sorted(map(lambda m: [m,
                                     self.movie_id_to_imdb_id_from_all_data[m],
                                     movie_id_to_title[m],
                                     self.deep_model.rate(user_id, m)],
                          movies),
                      key=lambda x: x[3],
                      reverse=True)

    def get_movie_recommendations(self, all_data, user_id, rec_count=5):
        return self.get_all_movie_recommendations(all_data, user_id)[:rec_count]

    def __get_movie_id_to_imdb_id(self):
        return {r[0]: 'tt%s' % r[1] for _, r in self.all_data[['movieId', 'imdbId']].iterrows()}
