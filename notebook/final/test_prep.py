import random

from utils import get_column


def get_uid_to_movie_count(all_data):
    uid_to_m_count = {}
    for index, row in all_data[['userId', 'movieId']].iterrows():
        if row[0] in uid_to_m_count:
            uid_to_m_count[row[0]] += 1
        else:
            uid_to_m_count[row[0]] = 1
    return uid_to_m_count


def get_sample_of_users(all_data, size=50):
    all_user_ids = set(get_column(all_data, 'userId'))
    return random.sample(all_user_ids, size)


def get_top_users_most_ratings(all_data, size=3):
    uid_to_m_count = get_uid_to_movie_count(all_data)
    return sorted([[k, v] for k, v in uid_to_m_count.items()], key=lambda x: x[1], reverse=True)[:size]


def get_top_users_fewest_ratings(all_data, size=3):
    uid_to_m_count = get_uid_to_movie_count(all_data)
    return sorted([[k, v] for k, v in uid_to_m_count.items()], key=lambda x: x[1])[:size]
