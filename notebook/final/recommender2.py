from utils import enhance_with_imdb_ratings


class Recommender2(object):
    def __init__(self, collab_model, content_model, imdb_ratings):
        self.collab_model = collab_model
        self.content_model = content_model
        self.imdb_id_to_imdb_rating = imdb_ratings

    def recommend(self, user_id, recommendations_count=10, rate_threshold=3.5, verbose=1, collab_recs=None):
        # Stage 1.1 - get movies rated >= 3.5
        if verbose:
            print 'Fetching movies to find the similar posters for...'
        m_ids, imdb_ids, dataset = self.collab_model.get_movies_rated_by(self.collab_model.all_data, user_id,
                                                                         str(rate_threshold))
        imdb_ids = map(lambda x: 'tt%s' % x, list(dataset.imdbId.unique()))
        # Return dataset here

        # Stage 1.2 - get poster recommendations for each of rated >= 3.5
        if verbose:
            print 'Fetching recommended movies (poster-based)...'
        all_poster_recs = set()
        for imdb_id in imdb_ids:
            _, recs = self.content_model.get_most_similar_movies_by_imdb_id(imdb_id, recommendations_count, display=0,
                                                                            verbose=0)
            if verbose:
                print 'Found recommendations for movie (ID = %s, title = %s)' % (
                    imdb_id, self.content_model.imdb_id_to_title[imdb_id])
            all_poster_recs |= set(recs)
        if verbose:
            print 'There are %d recommendation candidates from poster-based reco.' % len(all_poster_recs)

        # Stage 2.1 - get all collab recommendations, sorted descending by predicted rating
        if collab_recs is None:
            if verbose:
                print 'Fetching all collab recommendations for user (ID = %s)...' % user_id
            collab_recs = self.collab_model.get_all_movie_recommendations(self.collab_model.all_data, user_id)

        # Stage 2.2 - transform to imdb_id: rating dict
        if verbose:
            print 'Transforming to map IMDB ID -> rating...'
        collab_rates = {c[1]: c[3] for c in collab_recs}

        # Stage 3.1 - Sort poster-based recommendations by rating predicions
        recommended_imdb_ids = sorted(all_poster_recs, key=lambda x: collab_rates[x] if x in collab_rates else -1,
                                      reverse=True)[:recommendations_count]
        recommended_imdb_ids = map(lambda x: x[2:], recommended_imdb_ids)

        # Stage 3.2 - prepare dataset with recommendations
        recommended = self.content_model.movies_and_links.loc[
            self.content_model.movies_and_links['imdbId'].isin(recommended_imdb_ids)]

        # Stage 3.3 - enhance response with average ratings from IMDB
        if verbose:
            print 'Fetching IMDB average ratings for movies to recommend...'
        recommended = enhance_with_imdb_ratings(recommended, self.imdb_id_to_imdb_rating)
        rated_movies = enhance_with_imdb_ratings(
            self.collab_model.get_movies_rated_by(self.collab_model.all_data, user_id)[2],
            self.imdb_id_to_imdb_rating)

        if verbose:
            print 'Done!'
        return recommended, rated_movies, collab_rates
