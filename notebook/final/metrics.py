import time

from utils import load_imdb_ratings, get_all_data, log_time_left


class Metrics(object):
    def __init__(self, threshold=7.2):
        self.threshold = threshold
        self.accuracy = 0
        self.precision = 0
        self.recall = 0
        self.f1score = 0
        self.unrated_movies_count = 0

        self.tp = None
        self.fp = None
        self.tn = None
        self.fn = None

        self.recommended_no_img_count = 0
        self.recommended_no_rating_count = 0

        self.__all_movies_count = 9125

        self.__imdb_ratings = load_imdb_ratings()
        self.__positive_ratings = self.__get_positive_ratings()
        self.__negative_ratings = self.__get_negative_ratings()

        self.__imdb_ids_no_img = {'0108227', '0116992', '0422258', '3611354'}
        ratings, links, movies = get_all_data()
        unrated_movies = set(links.movieId.values) - set(ratings.movieId.values)
        self.__imdb_ids_unrated = set(links.loc[links['movieId'].isin(unrated_movies)].imdbId.values)

    def calculate(self, recommended_df, rated_df):
        self.unrated_movies_count = self.__all_movies_count - len(rated_df.imdbId.values)

        rec_ratings = recommended_df.imdbRating.values
        rec_ids = recommended_df.imdbId.values
        rat_ids = rated_df.imdbId.values

        self.recommended_no_img_count = len(filter(lambda x: x in self.__imdb_ids_no_img, rec_ids))
        self.recommended_no_rating_count = len(filter(lambda x: x in self.__imdb_ids_unrated, rec_ids))

        __tp_movies = filter(lambda x: x >= self.threshold, rec_ratings)
        self.tp = len(__tp_movies)

        __fp_movies = filter(lambda x: x < self.threshold, rec_ratings)
        self.fp = len(__fp_movies)

        __tn_movies = filter(lambda x: x not in rec_ids and x not in rat_ids, self.__negative_ratings)
        self.tn = len(__tn_movies)

        __fn_movies = filter(lambda x: x not in rec_ids and x not in rat_ids, self.__positive_ratings)
        self.fn = len(__fn_movies)

        self.accuracy = float(self.tp + self.tn) / (self.tp + self.tn + self.fp + self.fn)
        self.precision = float(self.tp) / (self.tp + self.fp)
        self.recall = float(self.tp) / (self.tp + self.fn)
        if self.precision + self.recall > 0:
            self.f1score = 2 * self.precision * self.recall / (self.precision + self.recall)
        else:
            self.f1score = 0

        return self

    def show_metrics(self):
        print 'Total number of movies not rated by user: %d' % self.unrated_movies_count
        print 'True positive (recommended, as expected):          %d' % self.tp
        print 'False positive (recommended, though not expected): %d' % self.fp
        print 'True negative (not recommended, as expected):      %d' % self.tn
        print 'False negative (not recommended, though expected): %d\n' % self.fn
        print 'Accuracy:  %.4f\n' \
              'Precision: %.4f\n' \
              'Recall:    %.4f\n' \
              'F1 Score:  %.4f\n' % (self.accuracy, self.precision, self.recall, self.f1score)
        print 'Recommended movies that have no poster image:      %d' % self.recommended_no_img_count
        print 'Recommended movies that have no rating in dataset: %d' % self.recommended_no_rating_count

        return self

    def __get_positive_ratings(self):
        ratings = {}
        for imdb_id, rating in self.__imdb_ratings.items():
            if rating >= self.threshold:
                ratings[imdb_id] = rating
        return ratings

    def __get_negative_ratings(self):
        ratings = {}
        for imdb_id, rating in self.__imdb_ratings.items():
            if rating < self.threshold:
                ratings[imdb_id] = rating
        return ratings


def avg(elements, extractor=None):
    if extractor is not None:
        return sum(map(extractor, elements)) / float(len(elements))
    return sum(elements) / float(len(elements))


def collect_metrics(user_ids, rec_call, recs_count=10, threshold=7.2, verbose=1, u_to_collab_recs=None):
    if verbose:
        print 'Starting to collect metrics for %d users.' % len(user_ids)
        print 'Total number of %d recommendations per user will be considered. Threshold for IMDB rating: %.1f' % (
            recs_count, threshold)
    metrics_list = []
    if u_to_collab_recs is None:
        u_to_collab_recs = {}
    else:
        print 'Reusing u_to_collab_recs for count = %d' % recs_count
    for u in user_ids:
        collab_recs = u_to_collab_recs[u] if u in u_to_collab_recs else None
        rec, rat, collab_recs = rec_call(u, recommendations_count=recs_count, verbose=verbose, collab_recs=collab_recs)
        if u not in u_to_collab_recs:
            u_to_collab_recs[u] = collab_recs
        metrics_list.append(Metrics(threshold).calculate(rec, rat))
        if verbose:
            print 'Collected metrics data for user (ID = %s)' % str(u)
    print 'Collected metrics from %d users for %d recommendations' % (len(user_ids), recs_count)
    return metrics_list, u_to_collab_recs


def aggregate_metrics(metrics_list, verbose=0):
    print len(metrics_list)
    # metrics_list[0].show_metrics()
    avg_unrated = avg(metrics_list, lambda x: x.unrated_movies_count)

    avg_tp = avg(metrics_list, lambda x: x.tp)
    avg_tn = avg(metrics_list, lambda x: x.tn)
    avg_fp = avg(metrics_list, lambda x: x.fp)
    avg_fn = avg(metrics_list, lambda x: x.fn)

    avg_acc = avg(metrics_list, lambda x: x.accuracy)
    avg_prec = avg(metrics_list, lambda x: x.precision)
    avg_rec = avg(metrics_list, lambda x: x.recall)
    avg_f1 = avg(metrics_list, lambda x: x.f1score)

    avg_rec_no_rating = avg(metrics_list, lambda x: x.recommended_no_rating_count)
    avg_rec_no_image = avg(metrics_list, lambda x: x.recommended_no_img_count)

    result = Metrics()
    result.unrated_movies_count = avg_unrated

    result.tp = avg_tp
    result.tn = avg_tn
    result.fp = avg_fp
    result.fn = avg_fn

    result.accuracy = avg_acc
    result.precision = avg_prec
    result.recall = avg_rec
    result.f1score = avg_f1

    result.recommended_no_rating_count = avg_rec_no_rating
    result.recommended_no_img_count = avg_rec_no_image

    if verbose:
        return result.show_metrics()
    return result


def collect_and_aggregate(recs_counts, user_ids, rec_call, threshold=7.2, verbose=0):
    rec_count_to_metrics = {}
    start = time.time()
    u_to_collab_recs = None
    for idx, count in enumerate(recs_counts):
        metrics_list, u_to_collab_recs = collect_metrics(user_ids,
                                                         rec_call,
                                                         count,
                                                         threshold,
                                                         verbose,
                                                         u_to_collab_recs)
        rec_count_to_metrics[count] = aggregate_metrics(metrics_list)
        log_time_left(idx, len(recs_counts), start, log_step_size=3)
    return rec_count_to_metrics
