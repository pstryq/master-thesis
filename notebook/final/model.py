import math
import os

import keras
import numpy as np
import pandas as pd
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import Dense, Dropout, Embedding, concatenate, Reshape, Input
from keras.optimizers import Adam

DEFAULT_DROPOUT = .2
MODEL_WEIGHTS_FILENAME_PREFIX = 'ml_small_weights'
WEIGHTS_FILE_EXTENSION = '.h5'


class DeepModel(object):
    def __init__(self,
                 n_users,
                 n_movies,
                 n_user_latent_factors,
                 n_movie_latent_factors,
                 dropout=DEFAULT_DROPOUT):
        self.dropout = dropout
        self.model = None
        self.history = None
        self.inputs_layer, self.outputs_layer = self.__create_inputs_layer(n_users,
                                                                           n_movies,
                                                                           n_user_latent_factors,
                                                                           n_movie_latent_factors,
                                                                           dropout)
        self.layers_count = 1

    def rate(self, user_id, movie_id):
        return self.model.predict([np.array([user_id]), np.array([movie_id])])[0][0]

    def add_relu_dense_layer(self, layer_size, dropout=DEFAULT_DROPOUT, name='FullyConnected'):
        return self.add_dense_layer(layer_size, dropout, name, 'relu')

    def add_dense_layer(self, layer_size, dropout=DEFAULT_DROPOUT, name='FullyConnected', activation=None):
        layer_name = self.__get_layer_name(name, self.layers_count)
        dropout_name = self.__get_dropout_layer_name(layer_name)

        self.outputs_layer = Dense(layer_size, name=layer_name, activation=activation)(self.outputs_layer)
        if dropout is not None:
            self.outputs_layer = Dropout(dropout, name=dropout_name)(self.outputs_layer)

        self.layers_count += 1
        return self

    def build(self, learning_rate, loss_function='mse'):
        adam = Adam(lr=learning_rate)
        self.model = keras.Model(self.inputs_layer, self.outputs_layer)
        self.model.compile(optimizer=adam, loss=loss_function)
        return self

    def predict(self, test_data, should_round=True):
        predicted = self.model.predict([test_data.userId, test_data.movieId])
        if should_round:
            predicted = self.__round_to_0_5(predicted)
        return test_data.rating, predicted

    def load_weights(self, suffix=''):
        self.model.load_weights(self.__get_weights_filepath(suffix))

    def fit(self, train_data, epochs=20, verbose=1, validation_split=.1, early_stop_patience=20,
            override_checkpoint=False, checkpoint_id=''):

        checkpoint_filepath = self.__get_weights_filepath(checkpoint_id)
        callbacks = [
            EarlyStopping('val_loss', patience=early_stop_patience),
            ModelCheckpoint(checkpoint_filepath, save_best_only=True)
        ]

        if override_checkpoint and os.path.isfile(checkpoint_filepath):
            print 'Checkpoint data will be overriden!'
            os.remove(checkpoint_filepath)

        self.history = self.model.fit([train_data.userId, train_data.movieId],
                                      train_data.rating,
                                      epochs=epochs,
                                      verbose=verbose,
                                      validation_split=validation_split,
                                      callbacks=callbacks)

    def plot_loss(self):
        loss = pd.DataFrame({'epoch': [i + 1 for i in self.history.epoch],
                             'training': [math.sqrt(loss) for loss in self.history.history['loss']],
                             'validation': [math.sqrt(loss) for loss in self.history.history['val_loss']]})
        loss_reindexed = loss.ix[:, :]
        xticks = list(loss_reindexed.epoch)
        ax = loss_reindexed.plot(x='epoch', grid=True, xticks=xticks)
        ax.set_ylabel("root mean squared error")
        ax.set_ylim([0.0, 2.0])
        return ax

    def get_min_validation_loss(self):
        min_val_loss, idx = min((val, idx) for (idx, val) in enumerate(self.history.history['val_loss']))
        print 'Minimum RMSE at epoch', '{:d}'.format(idx + 1), '=', '{:.4f}'.format(math.sqrt(min_val_loss))

    @staticmethod
    def __get_weights_filepath(suffix):
        if suffix != '':
            suffix = '_' + suffix
        return MODEL_WEIGHTS_FILENAME_PREFIX + suffix + WEIGHTS_FILE_EXTENSION

    @staticmethod
    def __round_to_0_5(values):
        return np.round(values * 2) / 2

    @staticmethod
    def __get_layer_name(name, layer_index):
        layer_name = name
        if name == 'FullyConnected':
            layer_name += '-%d' % layer_index
        return layer_name

    @staticmethod
    def __get_dropout_layer_name(layer_name):
        return '%s_Dropout' % layer_name

    def __create_inputs_layer(self, n_users, n_movies, n_user_latent_factors, n_movie_latent_factors, dropout):
        users_input_tensor, users_layer = self.__create_single_input_layer(n_users,
                                                                           n_user_latent_factors,
                                                                           'User',
                                                                           dropout)
        movies_input_tensor, movies_layer = self.__create_single_input_layer(n_movies,
                                                                             n_movie_latent_factors,
                                                                             'Movie',
                                                                             dropout)

        inputs_layer = concatenate([users_layer, movies_layer], name='Concat')
        if dropout > 0:
            inputs_layer = Dropout(dropout, name='Concat_Dropout')(inputs_layer)
        return [users_input_tensor, movies_input_tensor], inputs_layer

    @staticmethod
    def __create_single_input_layer(input_size, latent_factors, name, dropout):
        input_tensor = Input(shape=[1], name=name)
        input_layer = Embedding(input_size + 1, latent_factors, name='%s-Embedding' % name)(input_tensor)
        input_layer = Reshape((latent_factors,), name='Reshape%ss' % name)(input_layer)
        # input_layer = Flatten(name='Flatten%ss' % name)(input_layer)
        if dropout is not None:
            input_layer = Dropout(dropout, name='Dropout%ss' % name)(input_layer)
        return input_tensor, input_layer


from keras.optimizers import Adam
from keras.layers import Convolution1D, Activation, Flatten, Dropout, Dense
from keras import Sequential

nb_features = 20

model = Sequential()


def create_inputs_layer(self, n_users, n_movies, n_user_latent_factors, n_movie_latent_factors, dropout):
    users_input_tensor, users_layer = self.__create_single_input_layer(n_users,
                                                                       n_user_latent_factors,
                                                                       'User',
                                                                       dropout)
    movies_input_tensor, movies_layer = self.__create_single_input_layer(n_movies,
                                                                         n_movie_latent_factors,
                                                                         'Movie',
                                                                         dropout)

    inputs_layer = concatenate([users_layer, movies_layer], name='Concat')
    if dropout > 0:
        inputs_layer = Dropout(dropout, name='Concat_Dropout')(inputs_layer)
    return [users_input_tensor, movies_input_tensor], inputs_layer


def create_single_input_layer(input_size, latent_factors, name, dropout):
    input_tensor = Input(shape=[1], name=name)
    input_layer = Embedding(input_size + 1, latent_factors, name='%s-Embedding' % name)(input_tensor)
    input_layer = Reshape((latent_factors,), name='Reshape%ss' % name)(input_layer)
    # input_layer = Flatten(name='Flatten%ss' % name)(input_layer)
    if dropout is not None:
        input_layer = Dropout(dropout, name='Dropout%ss' % name)(input_layer)
    return input_tensor, input_layer
#
# u_input_tensor = Input(shape=[1])
# u_input_layer = Embedding(n_users + 1, latent_factors)(u_input_tensor)
# u_input_layer = Reshape((latent_factors,))(u_input_layer)
#
# m_input_tensor = Input(shape=[1])
# m_input_layer = Embedding(n_users + 1, latent_factors)(m_input_tensor)
# m_input_layer = Reshape((latent_factors,))(m_input_layer)
#
# inputs_layer = concatenate([u_input_layer, m_input_layer])
#
# conv_model = Convolution1D(nb_filter=512, filter_length=1, input_shape=(nb_features, 2), activation='relu')(inputs_layer)
# conv_model = Flatten()(conv_model)
# conv_model = Dropout(.2)(conv_model)
# conv_model = Dense(100, activation='relu')(conv_model)
# conv_model = Dense(1, activation='relu')(conv_model)
# adam = Adam(lr=.005)
# model.compile(loss='mse', optimizer=adam)