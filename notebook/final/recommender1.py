from utils import enhance_with_imdb_ratings


class Recommender1(object):
    def __init__(self, collab_model, content_model, imdb_ratings):
        self.collab_model = collab_model
        self.content_model = content_model
        self.imdb_id_to_imdb_rating = imdb_ratings

    def recommend(self, user_id, recommendations_count=10, rate_threshold=3.5, verbose=1, collab_recs=None):
        # Stage 1.1 - get all collab recommendations, sorted descending by predicted rating
        if collab_recs is None:
            if verbose:
                print 'Fetching all collab recommendations for user (ID = %s)...' % user_id
            collab_recs = self.collab_model.get_all_movie_recommendations(self.collab_model.all_data, user_id)

        # Stage 1.2 - transform to imdb_id: rating dict
        if verbose:
            print 'Transforming to map IMDB ID -> rating...'
        collab_rates = {c[1]: c[3] for c in collab_recs}

        # Stage 2.1 - get movies rated >= 3.5
        if verbose:
            print 'Fetching movies to find the similar posters for...'
        m_ids, imdb_ids, dataset = self.collab_model.get_movies_rated_by(self.collab_model.all_data, user_id,
                                                                         str(rate_threshold))
        imdb_ids = map(lambda x: 'tt%s' % x, list(dataset.imdbId.unique()))
        # Return dataset here

        # Stage 2.2 - get poster recommendations for each of rated >= 3.5
        if verbose:
            print 'Fetching recommended movies (poster-based)...'
        imdb_id_to_poster_recs = {}
        for imdb_id in imdb_ids:
            _, recs = self.content_model.get_most_similar_movies_by_imdb_id(imdb_id, recommendations_count, display=0,
                                                                            verbose=0)
            if verbose:
                print 'Found recommendations for movie (ID = %s, title = %s)' % (
                    imdb_id, self.content_model.imdb_id_to_title[imdb_id])
            imdb_id_to_poster_recs[imdb_id] = recs

        # Stage 2.3 - calculate points for each of content-recommended movie
        if verbose:
            print 'Calculating points from poster-based reco...'
        imdb_id_to_points = {}
        for imdb_id, recs in imdb_id_to_poster_recs.iteritems():
            w = float(dataset.loc[dataset['imdbId'] == imdb_id[2:]].rating)
            for i, r in enumerate(recs):
                s_i = w * (float(recommendations_count) - i) / recommendations_count
                if r in imdb_id_to_points:
                    imdb_id_to_points[r] += s_i
                else:
                    imdb_id_to_points[r] = s_i

        # Stage 3.1 - gather all recommended movies (just IDs)
        collab_reco_ids = set(collab_rates.keys())
        content_reco_ids = set(imdb_id_to_points.keys())
        if verbose:
            print 'There are %d recommendations from poster-based reco.' % len(content_reco_ids)
            print 'There are %d recommendations from collaborative reco.' % len(collab_reco_ids)

        all_recommended_imdb_ids = content_reco_ids | collab_reco_ids
        if verbose:
            print 'Gathered all recommendation candidates - there are %d of them so far!' % len(
                all_recommended_imdb_ids)

        # Stage 3.2 - merge recommendations from collab and poster-based
        if verbose:
            print 'Merging recommendations from collab and poster-based...'
        all_recs_summarised = []
        for r in all_recommended_imdb_ids:
            final_rate = 0
            if r in collab_rates:
                final_rate += .7 * collab_rates[r]
            if r in imdb_id_to_points:
                final_rate += .3 * imdb_id_to_points[r]
            all_recs_summarised.append([r, final_rate])
        all_recs_summarised = sorted(all_recs_summarised, key=lambda x: x[1], reverse=True)

        # Stage 3.3 - retrieve just IDs of recommended movies - without 'tt'
        if verbose:
            print 'Preparing response...'
        recommended_imdb_ids = map(lambda x: x[0][2:], all_recs_summarised[:recommendations_count])

        # Stage 3.4 - prepare dataset with recommendations
        recommended = self.content_model.movies_and_links.loc[
            self.content_model.movies_and_links['imdbId'].isin(recommended_imdb_ids)]

        # Stage 3.5 - enhance response with average ratings from IMDB
        if verbose:
            print 'Fetching IMDB average ratings for movies to recommend...'
        recommended = enhance_with_imdb_ratings(recommended, self.imdb_id_to_imdb_rating)
        rated_movies = enhance_with_imdb_ratings(
            self.collab_model.get_movies_rated_by(self.collab_model.all_data, user_id)[2],
            self.imdb_id_to_imdb_rating)

        if verbose:
            print 'Done!'
        return recommended, rated_movies, collab_recs
