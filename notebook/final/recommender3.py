from utils import enhance_with_imdb_ratings


class Recommender3(object):
    def __init__(self, collab_model, content_model, imdb_ratings):
        self.collab_model = collab_model
        self.content_model = content_model
        self.imdb_id_to_imdb_rating = imdb_ratings
        self.m_imdb_id_to_u_count = self.get_imdb_id_to_rating_count()

    def recommend(self, user_id, recommendations_count=10, rate_threshold=3.5, verbose=1, collab_recs=None):
        # Stage 0.1 - get all movies already rated by user
        if verbose:
            print 'Fetching all movies already rated by user (ID = %s)...' % user_id
        rated_by_user = self.collab_model.get_movies_rated_by(self.collab_model.all_data, user_id)

        # Stage 1.1 - get all collab recommendations, sorted descending by predicted rating
        if collab_recs is None:
            if verbose:
                print 'Fetching all collab recommendations for user (ID = %s)...' % user_id
            collab_recs = self.collab_model.get_all_movie_recommendations(self.collab_model.all_data, user_id)

        # Stage 1.2 - get first (recommendations_count / 2) recommendations already
        first_collab_recs_count = recommendations_count / 2
        all_recommended_imdb_ids = map(lambda x: x[1], collab_recs[:first_collab_recs_count])

        # Stage 1.3 - transform to imdb_id: rating dict
        if verbose:
            print 'Transforming to map IMDB ID -> rating...'
        collab_rates = {c[1]: c[3] for c in collab_recs}

        # Stage 2.1 - get movies rated >= 3.5
        if verbose:
            print 'Fetching movies to find the similar posters for...'
        m_ids, imdb_ids, dataset = self.collab_model.get_movies_rated_by(self.collab_model.all_data, user_id,
                                                                         str(rate_threshold))
        imdb_ids = map(lambda x: 'tt%s' % x, list(dataset.imdbId.unique()))

        # Stage 2.2 - get poster recommendations for each of rated >= 3.5
        if verbose:
            print 'Fetching recommended movies (poster-based)...'
        poster_imdb_ids = set()
        for imdb_id in imdb_ids:
            _, recs = self.content_model.get_most_similar_movies_by_imdb_id(imdb_id, recommendations_count, display=0,
                                                                            verbose=0)
            if verbose:
                print 'Found recommendations for movie (ID = %s, title = %s)' % (
                    imdb_id, self.content_model.imdb_id_to_title[imdb_id])
            poster_imdb_ids |= set(recs)

        # Stage 2.3 - remove ids of collab candidates and already rated movies
        if verbose:
            print 'Fine-tuning candidates - removing already collab-recommended movies and already rated ones...'
        poster_imdb_ids -= set(all_recommended_imdb_ids)
        poster_imdb_ids -= set(rated_by_user[1])

        # Stage 2.4 - sort by number of ratings, ascending
        if verbose:
            print 'Sorting recommendations by number of ratings...'
        poster_recs_ids = sorted(poster_imdb_ids, key=lambda x: self.m_imdb_id_to_u_count[x[2:]])
        poster_candidates_count = recommendations_count - first_collab_recs_count
        if len(poster_recs_ids) < poster_candidates_count:
            if verbose:
                print '%d recommendations from posters will be added' % len(poster_recs_ids)
            # Add all from poster recos, fill the rest with collab recos
            all_recommended_imdb_ids.extend(poster_recs_ids)

            additional_collab_recs_count = poster_candidates_count - len(poster_recs_ids)
            collab_recs_ids = map(lambda x: x[1],
                                  collab_recs[
                                  first_collab_recs_count:first_collab_recs_count + additional_collab_recs_count])
            if verbose:
                print 'Additionally, %d recommendations from collab will be added' % len(collab_recs_ids)
            all_recommended_imdb_ids.extend(collab_recs_ids)

        elif len(poster_recs_ids) > poster_candidates_count and poster_recs_ids[poster_candidates_count - 1] == \
                poster_recs_ids[poster_candidates_count]:
            poster_conflict_end_idx = poster_candidates_count
            while poster_conflict_end_idx < len(poster_recs_ids) and \
                    poster_recs_ids[poster_conflict_end_idx - 1] == poster_recs_ids[poster_conflict_end_idx]:
                poster_conflict_end_idx += 1

            poster_conflict_start_idx = poster_candidates_count - 1
            while poster_conflict_start_idx > 0 and \
                    poster_recs_ids[poster_conflict_start_idx - 1] == poster_recs_ids[poster_conflict_start_idx]:
                poster_conflict_start_idx -= 1

            if verbose:
                print 'Unambigous candidate ordering detected, from %d to %d' % (
                poster_conflict_start_idx, poster_conflict_end_idx)
            if poster_conflict_start_idx > 0:
                if verbose:
                    print 'Adding %d recommendations from posters so far' % poster_conflict_start_idx
                all_recommended_imdb_ids.extend(poster_recs_ids[:poster_conflict_start_idx])

            conflicting_poster_ids_by_rating = sorted(
                poster_recs_ids[poster_conflict_start_idx:poster_candidates_count],
                reverse=True,
                key=lambda x: collab_rates[x])
            all_recommended_imdb_ids.extend(
                conflicting_poster_ids_by_rating[:poster_candidates_count - poster_conflict_start_idx])
        else:
            all_recommended_imdb_ids.extend(poster_recs_ids[:poster_candidates_count])

        # Stage 3.3 - retrieve just IDs of recommended movies - without 'tt'
        if verbose:
            print 'Preparing response...'
        recommended_imdb_ids = map(lambda x: x[2:], all_recommended_imdb_ids[:recommendations_count])

        # Stage 3.4 - prepare dataset with recommendations
        recommended = self.content_model.movies_and_links.loc[
            self.content_model.movies_and_links['imdbId'].isin(recommended_imdb_ids)]

        # Stage 3.5 - enhance response with average ratings from IMDB
        if verbose:
            print 'Fetching IMDB average ratings for movies to recommend...'
        recommended = enhance_with_imdb_ratings(recommended, self.imdb_id_to_imdb_rating)
        rated_movies = enhance_with_imdb_ratings(
            self.collab_model.get_movies_rated_by(self.collab_model.all_data, user_id)[2],
            self.imdb_id_to_imdb_rating)

        if verbose:
            print 'Done!'
        return recommended, rated_movies, collab_recs

    def get_imdb_id_to_rating_count(self):
        unrated_movies = set(self.content_model.movies_and_links.imdbId.unique()) - set(
            self.collab_model.all_data.imdbId.unique())
        m_imdb_id_to_u_count = {}

        for _, row in self.collab_model.all_data[['imdbId']].iterrows():
            if row[0] in m_imdb_id_to_u_count:
                m_imdb_id_to_u_count[row[0]] += 1
            else:
                m_imdb_id_to_u_count[row[0]] = 1

        for m in unrated_movies:
            m_imdb_id_to_u_count[m] = 0
        return m_imdb_id_to_u_count
