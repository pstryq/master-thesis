import pandas as pd
import numpy as np
from cPickle import dump, load

import os
import time
from IPython.display import SVG
from keras.utils.vis_utils import model_to_dot
from sklearn.metrics import mean_absolute_error as mae, mean_squared_error as mse

MOVIELENS_PATH = '/Users/patrykkowalczyk/Desktop/ml-latest-small/'
RATINGS_FILE = 'ratings.csv'
LINKS_FILE = 'links.csv'
MOVIES_FILE = 'movies.csv'
TAGS_FILE = 'tags.csv'

RATINGS_HEADERS = 'userId,movieId,rating,timestamp'
LINKS_HEADERS = 'movieId,imdbId,tmdbId'
MOVIES_HEADERS = 'movieId,title,genres'
TAGS_HEADERS = 'userId,movieId,tag,timestamp'


def __get_data(filename, headers, verbose=0):
    path = MOVIELENS_PATH + filename
    if verbose:
        print 'Getting data from %s' % path
    return pd.read_csv(path, sep=',', names=headers.split(","))[1:]


def get_ratings(verbose=0):
    return __get_data(RATINGS_FILE, RATINGS_HEADERS, verbose)


def get_links(verbose=0):
    return __get_data(LINKS_FILE, LINKS_HEADERS, verbose)


def get_movies(verbose=0):
    return __get_data(MOVIES_FILE, MOVIES_HEADERS, verbose)


def get_tags(verbose=0):
    return __get_data(TAGS_FILE, TAGS_HEADERS, verbose)


def get_all_data(verbose=0):
    return get_ratings(verbose), get_links(verbose), get_movies(verbose)


def get_all_data_merged(verbose=0):
    ratings, links, movies = get_all_data(verbose)

    merged = pd.merge(ratings, links, on='movieId', how='left')
    all_data = pd.merge(merged, movies, on='movieId', how='left')
    all_data = all_data.loc[all_data['imdbId'].notnull()]

    all_data['movieId'] = merged['imdbId']
    all_data.userId = merged.userId.astype('category').cat.codes.values
    all_data.movieId = merged.movieId.astype('category').cat.codes.values

    return all_data


def get_ratings_and_links_merged(verbose=0):
    ratings, links = get_ratings(verbose), get_links(verbose)

    ratings_and_links = pd.merge(ratings, links, on='movieId', how='left')
    ratings_and_links = ratings_and_links.loc[ratings_and_links['imdbId'].notnull()]

    ratings_and_links['movieId'] = ratings_and_links['imdbId']
    ratings_and_links.userId = ratings_and_links.userId.astype('category').cat.codes.values
    ratings_and_links.movieId = ratings_and_links.movieId.astype('category').cat.codes.values

    print_movies_with_at_least_one_rating(ratings_and_links)
    print_users_with_at_least_one_rating(ratings_and_links)

    return ratings_and_links


def get_movies_and_links_merged(verbose=0):
    movies, links = get_movies(verbose), get_links(verbose)

    movies_and_links = pd.merge(movies, links, on='movieId', how='left')
    movies_and_links = movies_and_links.loc[movies_and_links['imdbId'].notnull()]
    return movies_and_links


def get_users_and_movies_count(dataset):
    n_users, n_movies = len(dataset.userId.unique()), len(dataset.movieId.unique())
    print 'Number of users: %d\nNumber of rated movies: %d' % (n_users, n_movies)
    return n_users, n_movies


def get_imdb_url(imdb_id):
    return 'http://www.imdb.com/title/%s/?ref_=fn_tt_tt_1' % imdb_id


def get_column(dataset, column_id):
    column = dataset[column_id].values
    print '%s: %s, shape = %s' % (column_id, str(column), str(column.shape))
    return column


def get_metrics(y_true, y_predicted):
    mean_abs_err = mae(y_true, y_predicted)
    mean_sq_err = mse(y_true, y_predicted)
    print 'Mean absolute error: %.4f\nMean squared error: %.4f' % (mean_abs_err, mean_sq_err)
    return mean_abs_err, mean_sq_err


def draw_model(deep_model):
    return SVG(model_to_dot(deep_model.model, show_shapes=True, show_layer_names=True, rankdir='HB')
               .create(prog='dot', format='svg'))


def summarise_model(deep_model):
    return deep_model.model.summary()


def compare_models(id_to_model, test_data):
    for model_id, model in id_to_model.iteritems():
        y_true, y_predicted = model.predict(test_data)
        mean_abs_err, mean_sq_err = get_metrics(y_true, y_predicted)

        print 'Model(ID = %s): mean abs error = %f, mean sq error = %f' % (model_id, mean_abs_err, mean_sq_err)


def print_users_with_at_least_one_rating(dataset):
    user_ids = dataset['userId'].drop_duplicates()
    print len(user_ids), 'of the', user_ids.max() + 1, 'users rate at least one movie.'


def print_movies_with_at_least_one_rating(dataset):
    movie_ids = dataset['movieId'].drop_duplicates()
    print len(movie_ids), 'of the', movie_ids.max() + 1, 'movies are rated.'


def save_predictions_matrix(matrix):
    save_in_file(matrix, 'matrix')


def load_predictions_matrix():
    return load_from_file('matrix')


def save_in_file(data_to_save, file_name):
    dump(data_to_save, open('%s.p' % file_name, 'wb'))


def load_img_files():
    return map(lambda x: x[:-4], os.listdir('img'))


def load_imdb_ratings():
    return load_from_file('imdb_ratings')


def load_similarity_matrix():
    return load_from_file('similarity')


def load_from_file(file_name):
    return load(open('%s.p' % file_name, 'rb'))


def log_time_left(step, total_steps, start_time, log_step_size=100):
    if (step + 1) % 100 == 0:
        interval = time.time() - start_time
        left = float(interval) / (step + 1) * total_steps - interval
        print 'So far %d done in %d seconds , estimated time left: %dh %dm %ds' % (step + 1,
                                                                                   interval,
                                                                                   left / 3600,
                                                                                   (left / 60) % 60,
                                                                                   left % 60)


def has_corresponding_image(imdb_id):
    return os.path.isfile('img/tt' + str(imdb_id) + '.jpg')


def reverse_dict(d):
    return {v: k for k, v in d.iteritems()}


def to_df(data, columns):
    return pd.DataFrame(data=np.array(data), columns=columns)


def enhance_with_imdb_ratings(dataframe, id_to_rating):
    try:
        imdb_id_col_idx = dataframe.columns.get_loc("imdbId")
        dataframe['imdbRating'] = dataframe.apply(lambda row: id_to_rating[row[imdb_id_col_idx]], axis=1)
    except KeyError as e:
        print 'Column \'imdbId\' is not present in provided dataset!'
    return dataframe
