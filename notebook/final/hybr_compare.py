from collab_model import CollaborativeModel
from content_model import ContentBasedModel
from recommender1 import Recommender1
from recommender2 import Recommender2
from recommender3 import Recommender3
from utils import load_imdb_ratings
from model import DeepModel


def load_collab_model(model_id='DEEP4_FINAL'):
    collab_model = CollaborativeModel()

    LATENT_FACTORS_USERS = 40
    LATENT_FACTORS_MOVIES = 40
    LEARNING_RATE = .005
    DROPOUT = .2

    collab_deep_model = DeepModel(collab_model.n_users,
                                  collab_model.n_movies,
                                  LATENT_FACTORS_USERS,
                                  LATENT_FACTORS_MOVIES,
                                  DROPOUT) \
        .add_relu_dense_layer(80) \
        .add_relu_dense_layer(20) \
        .add_relu_dense_layer(1, 'Activation', None) \
        .build(LEARNING_RATE)

    collab_model.load_model(collab_deep_model, model_id)
    return collab_model


collab_model = load_collab_model()
content_model = ContentBasedModel()
imdb_ratings = load_imdb_ratings()

recommender1 = Recommender1(collab_model, content_model, imdb_ratings)
recommender2 = Recommender2(collab_model, content_model, imdb_ratings)
recommender3 = Recommender3(collab_model, content_model, imdb_ratings)
