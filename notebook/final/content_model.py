import os
import numpy as np
from IPython.core.display import HTML, display

from utils import get_movies_and_links_merged, has_corresponding_image, reverse_dict, load_similarity_matrix


class ContentBasedModel(object):

    def __init__(self):
        print 'Loading movies and links...'
        self.movies_and_links = get_movies_and_links_merged()

        print 'Loading link ids...'
        self.link_ids = map(str, filter(has_corresponding_image, self.movies_and_links.imdbId.unique()))

        print 'Loading IMDB id -> title translations...'
        self.imdb_id_to_title = {'tt' + row.imdbId: row.title for row in self.movies_and_links.itertuples()
                                 if row.imdbId in self.link_ids}

        print 'Loading posters...'
        self.img_files = map(lambda x: x[:-4], os.listdir('img'))
        self.total_movies = len(self.img_files)

        print 'Loading movie id <-> IMDB id translations...'
        self.__idx_to_imdb_id = {i: e for i, e in enumerate(self.img_files)}
        self.__imdb_id_to_idx = reverse_dict(self.__idx_to_imdb_id)

        print 'Loading similarity matrix...'
        self.similarity_matrix = load_similarity_matrix()
        print 'Finished!'

    def find_candidates(self, title, verbose=1):
        candidates = []
        for imdb_id, v in self.imdb_id_to_title.iteritems():
            if title in v:
                movie_idx_from_img_files = self.__imdb_id_to_idx[imdb_id]
                title = self.imdb_id_to_title[imdb_id]
                candidate = (
                    movie_idx_from_img_files,
                    imdb_id,
                    title
                )
                candidates.append(candidate)
                if verbose:
                    print 'Index: %s, imdbId: %s, title: %s' % candidate
        return candidates

    def get_most_similar_movies(self, movie_idx, results_count, display=1, verbose=1):
        movies = []
        i = 0
        for x in np.argsort(self.similarity_matrix[movie_idx, :])[:1:-1]:
            if x != movie_idx and x in self.__idx_to_imdb_id:
                movies.append(self.__idx_to_imdb_id[x])
                i += 1
            if i == results_count:
                break
        if verbose == 1:
            print movies

        if display:
            self.__display_similar_movies(movie_idx, movies)

        return self.__idx_to_imdb_id[movie_idx], movies

    def get_most_similar_movies_by_imdb_id(self, imdb_id, results_count, display=1, verbose=1):
        movie_idx = self.__imdb_id_to_idx[imdb_id]
        return self.get_most_similar_movies(movie_idx, results_count, display, verbose)

    def __display_similar_movies(self, movie_idx, movies):
        imdb_ids = [str(movie) for movie in movies]
        images_html = ''
        print 'Similar movie posters for ID = %s: %s' % (
            self.__idx_to_imdb_id[movie_idx], self.imdb_id_to_title[self.__idx_to_imdb_id[movie_idx]])
        images_html += "<img style='width: 150px; margin: 0px; \
                       float: left; border: 1px solid black;' \
                       src='img/%s.jpg' />   " \
                       % self.__idx_to_imdb_id[movie_idx]
        for imdb_id in imdb_ids:
            print imdb_id, self.imdb_id_to_title[imdb_id] if imdb_id in self.imdb_id_to_title else 'Unknown'
            images_html += "<img style='width: 150px; margin: 0px; \
                       float: left; border: 1px solid black;' \
                       src='img/%s.jpg' />" \
                           % imdb_id
        html = HTML(images_html)
        display(html)
